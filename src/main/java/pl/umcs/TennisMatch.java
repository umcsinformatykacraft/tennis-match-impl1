package pl.umcs;

public class TennisMatch {

    private Player firstPlayer;
    private Player secondPlayer;

    private TennisGameScore currentGameScore = new TennisGameScore(0, 0);

    public TennisMatch(Player player1, Player player2) {
        this.firstPlayer = player1;
        this.secondPlayer = player2;
    }


    public Player currentServer(){
        return firstPlayer;
    }

    public TennisGameScore currentGameScore(){
        return currentGameScore;
    }

    public void pointFinished(Player winner){
        if(winner == firstPlayer){
            currentGameScore = currentGameScore.firstPlayerWonThePoint();
        }
        else{
            currentGameScore = currentGameScore.secondPlayerWonThePoint();
        }
    }
}
