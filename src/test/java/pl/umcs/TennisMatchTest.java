package pl.umcs;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TennisMatchTest {

    private Player player1 = new Player();
    private Player player2 = new Player();
    private TennisMatch tennisMatch = new TennisMatch(player1, player2);

    @Test
    public void shouldReturnFirstServerOnMatchBeginning()
    {
        //when
        Player returnedPlayer = tennisMatch.currentServer();

        //then
        assertEquals(returnedPlayer, player1);
    }

    @Test
    public void shouldReturnZeroScoreOnMatchBeginning()
    {
        //when
        TennisGameScore currentGameScore = tennisMatch.currentGameScore();

        //then
        assertEquals( new TennisGameScore(0,0), currentGameScore);
    }

    @Test
    public void shouldReturnOneToZeroPoint()
    {
        //when
        tennisMatch.pointFinished(player1);
        TennisGameScore currentGameScore = tennisMatch.currentGameScore();

        //then
        assertEquals( new TennisGameScore(1,0), currentGameScore);
    }

    @Test
    public void shouldReturnTwoToThreeScore()
    {
        //when
        tennisMatch.pointFinished(player1);
        tennisMatch.pointFinished(player1);
        tennisMatch.pointFinished(player2);
        tennisMatch.pointFinished(player2);
        tennisMatch.pointFinished(player2);
        TennisGameScore currentGameScore = tennisMatch.currentGameScore();

        //then
        assertEquals( new TennisGameScore(2,3), currentGameScore);
    }
}
